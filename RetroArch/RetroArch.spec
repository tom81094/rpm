Name:           RetroArch
Version:        1.7.5
Release:        2%{?dist}
Summary:        Cross-platform, sophisticated frontend for the libretro API. Licensed GPLv3

License:        GPLv3
URL:            http://www.libretro.com/
Source0:        https://github.com/libretro/%{name}/archive/v%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  freetype-devel
BuildRequires:  libXinerama-devel
BuildRequires:  libusb-devel
BuildRequires:  libv4l-devel
BuildRequires:  libxkbcommon-devel
BuildRequires:  libxml2-devel
BuildRequires:  mesa-libEGL-devel
BuildRequires:  mesa-libgbm-devel
BuildRequires:  openal-soft-devel
BuildRequires:  perl-Net-DBus
BuildRequires:  perl-X11-Protocol
BuildRequires:  pulseaudio-libs-devel
BuildRequires:  SDL-devel
BuildRequires:  SDL2-devel
BuildRequires:  vulkan-devel
BuildRequires:  zlib-devel

Requires:       alsa-lib
Requires:       bzip2-libs
Requires:       freetype
Requires:       libX11
Requires:       libXau
Requires:       libXext
Requires:       libXxf86vm
Requires:       libglvnd-egl
Requires:       libglvnd-glx
Requires:       libpng
Requires:       libxcb
Requires:       pulseaudio-libs
Requires:       SDL2
Requires:       vulkan
Requires:       zlib

%description
RetroArch is the reference frontend for the libretro API. Popular examples of implementations for this API includes videogame system emulators and game engines, but also more generalized 3D programs. These programs are instantiated as dynamic libraries. We refer to these as "libretro cores".

%global debug_package %{nil}

%prep
%autosetup

%build
./configure --prefix=/usr
%make_build

%install
rm -rf %{buildroot}
%make_install

%files
%license COPYING
%doc CHANGES.md CONTRIBUTING.md README.md README-exynos.md README-OMAP.md README-mali_fbdev_r4p0.md
%{_bindir}/retroarch
%{_bindir}/retroarch-cg2glsl
%{_datadir}/applications/retroarch.desktop
%{_datadir}/pixmaps/retroarch.svg
%{_mandir}/man6/retroarch-cg2glsl.6.gz
%{_mandir}/man6/retroarch.6.gz
%{_sysconfdir}/retroarch.cfg
/usr/share/doc/retroarch/COPYING
/usr/share/doc/retroarch/README.md


%changelog
* Sun Nov 4 2018 monotykamary
- Added gcc-c++ as BuildRequres for F29/Rawhide+

* Wed Oct 31 2018 monotykamary
- Update to Retroarch 1.7.5

* Mon Sep 24 2018 monotykamary
- Added gcc as BuildRequres for F29/Rawhide+

* Mon Sep 24 2018 monotykamary
- Updated source download

* Tue Sep 11 2018 monotykamary
- Update to RetroArch 1.7.4

* Tue May 8 2018 monotykamary
- Update to RetroArch 1.7.3

* Wed Apr 25 2018 monotykamary
- Update to RetroArch 1.7.2

* Sun Feb 25 2018 monotykamary
- Added vulkan buildrequires from upstream spoonsauce repo

* Sat Feb 17 2018 monotykamary
- Update to RetroArch 1.7.1

* Tue Dec 26 2017 monotykamary
- Update to RetroArch 1.7.0

* Wed Nov 22 2017 monotykamary
- Update to RetroArch 1.6.9

* Sun Nov 19 2017 monotykamary
- Update to RetroArch 1.6.7

* Fri Jul 28 2017 spoonsauce
- Update to RetroArch 1.6.3
- Add Fedora 25 support (i386 and x86_64)
- Add i386 build for Fedora 26

* Wed Jul 19 2017 spoonsauce
- Initial import
