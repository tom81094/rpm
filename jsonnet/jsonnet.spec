%define _hardened_build 1

Name:              jsonnet
Version:           0.12.1
Release:           1
Summary:           Jsonnet is a domain specific configuration language that helps you define JSON data
URL:               http://jsonnet.org/
Source0:           https://codeload.github.com/google/%{name}/tar.gz/v%{version}

License:           ASL 2.0

BuildRequires: gcc
BuildRequires: libgcc
BuildRequires: gcc-c++

BuildRequires: python2-devel
%if 0%{?fedora}
BuildRequires: python3-devel
%endif

%if 0%{?rhel}
BuildRequires: python-setuptools
%endif

%description
Jsonnet is a domain specific configuration language that helps you define
JSON data. Jsonnet lets you compute fragments of JSON within the structure,
bringing the same benefit to structured data that templating languages
bring to plain text.
Use Jsonnet to organize your JSON data, or to help your users organize the data
they send to you. It is easy to integrate Jsonnet library or commandline
interpreter into existing systems that take JSON or YAML. Some example
situations are application configuration (files in etc), build systems,
package definitions, configuration management systems, and cloud deployment
agents.

%package -n lib%{name}
Summary: Jsonnet library

%description -n lib%{name}
Allows Jsonnet to be used as a library.

%package -n lib%{name}-devel
Summary: Development headers for Jsonnet
Requires: lib%{name}%{?_isa} = %{version}-%{release}

%description -n lib%{name}-devel
Development headers for lib%{name} for C and C++

%package -n lib%{name}-python2
Requires: lib%{name}%{?_isa} = %{version}-%{release}
Summary: Python 2 API for Jsonnet

%description -n lib%{name}-python2
Allows Jsonnet to be used within Python 2.

%if 0%{?fedora}
%package -n lib%{name}-python3
Requires: lib%{name}%{?_isa} = %{version}-%{release}
Summary: Python 3 API for Jsonnet

%description -n lib%{name}-python3
Allows Jsonnet to be used within Python 3.
%endif

%prep
%setup -q -n %{name}-%{version}

%build
make
make lib%{name}.so
make lib%{name}++.so
%py2_build
%if 0%{?fedora}
%py3_build
%endif

%check
make test

%install
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_includedir}
mkdir -p $RPM_BUILD_ROOT%{_libdir}

%py2_install
%if 0%{?fedora}
%py3_install
%endif

install -m 0755 -s ./jsonnet $RPM_BUILD_ROOT%{_bindir}
install -m 0644 ./include/* $RPM_BUILD_ROOT%{_includedir}
install -m 0755 -s ./lib%{name}.so $RPM_BUILD_ROOT%{_libdir}
install -m 0755 -s ./lib%{name}++.so $RPM_BUILD_ROOT%{_libdir}

%files
%{_bindir}/jsonnet
%license LICENSE

%files -n lib%{name}
%{_libdir}/*
%license LICENSE

%files -n lib%{name}-python2
%{python2_sitearch}/*

%if 0%{?fedora}
%files -n lib%{name}-python3
%{python3_sitearch}/*
%endif

%files -n lib%{name}-devel
%{_includedir}/*

%changelog
* Tue Dec 25 2018 Tom Nguyen <tom81094@gmail.com> 0.12.1-1
- Updated jsonnet to 0.12.1

* Fri Jul 20 2018 Tom Nguyen <tom81094@gmail.com> 0.11.2-2
- Added gcc, libgcc, and gcc-c++ as required builds for rawhide

* Fri Jul 20 2018 Tom Nguyen <tom81094@gmail.com> 0.11.2-1
- Updated jsonnet to 0.11.2

* Wed Jun 6 2018 Tom Nguyen <tom81094@gmail.com> 0.10.0-1
- Forked from randomvariable/jsonnet
- Updated jsonnet to 0.10.0

* Tue Feb 13 2018 Naadir Jeewa <naadir@randomvariable.co.uk> 0.9.5-4
- Include and correct license in libjsonnet (#1542522)
- Match Fedora/EPEL standards for file permissions (#1542522)

* Sat Feb 10 2018 Naadir Jeewa <naadir@randomvariable.co.uk> 0.9.5-3
- Update spec to match Fedora packaging standards (#1542522)
- jsonnet-* packages renamed to libjsonnet-* (#1542522)

* Tue Feb 06 2018 Naadir Jeewa <naadir@randomvariable.co.uk> 0.9.5-2
- Update package summaries (naadir@randomvariable.co.uk)
- Add build badge (naadir@randomvariable.co.uk)

* Sat Nov 25 2017 Naadir Jeewa <naadir@randomvariable.co.uk> 0.9.5-1
- new package built with tito


